﻿using System;

namespace ConsoleApplication1
{

    class Program
    {
        static void Main(string[] args)
        {
            using (var spammer = Spammer.CreateSpammer())
            {
                Console.WriteLine("Enter q to exit:");
                while (Console.ReadKey().KeyChar != 'q') ;
            }
        }
    }
}
