﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace ConsoleApplication1
{
    public class Spammer : IDisposable
    {
        private const int keyCodeToSpam = 0x60;
        private static StreamReader _reader;
        private const string directory = @"D:\Not Aion 5.8\";
        private const string filename = "Chat.log";

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        private const int KEYEVENTF_EXTENDEDKEY = 0x0001; //Key down flag
        private const int KEYEVENTF_KEYUP = 0x0002; //Key up flag

        private void Spam(int sleep)
        {
            Thread.Sleep(sleep);
            int start = Environment.TickCount;

            const int nChars = 256;
            StringBuilder appName = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();
            if (GetWindowText(handle, appName, nChars) == 0)
                return;
            if (appName.ToString() != "AION")
                return;
            while (Environment.TickCount - start < 350)
            {
                keybd_event(keyCodeToSpam, 0, KEYEVENTF_EXTENDEDKEY, 0);
                keybd_event(keyCodeToSpam, 0, KEYEVENTF_KEYUP, 0);
                Thread.Sleep(15);
            }
        }
        
        public static Spammer CreateSpammer()
        {
            return new Spammer();
        }

        private Spammer()
        {
            FileStream fs = File.Open(directory + filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            _reader = new StreamReader(fs);
            _reader.ReadToEnd();

            // Create a new FileSystemWatcher and set its properties.
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = directory;
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite;
            watcher.Filter = filename;

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }


        private void OnChanged(object source, FileSystemEventArgs e)
        {
            string str;
            while (!_reader.EndOfStream)
            {
                str = string.Empty;
                str = _reader.ReadLine();
                Thread t;
                if (str.Contains("[charname:"))
                    continue;
                else if (str.Contains("has made you afraid by using Nightmare Scream."))
                    t = new Thread(() => Spam(250));
                else if (str.Contains("transformed you into a(n) Slumbering Lamb by using Slumberswept Wind."))
                    t = new Thread(() => Spam(500));
                else
                    continue;
                t.Start();
                Console.WriteLine(str);
            }
        }

        public void Dispose()
        {
            _reader.Close();
            GC.SuppressFinalize(this);
        }
    }
}
